const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
const Message = require('../models/msgModel')



router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())

// response format: positive
const responseTrue = (data) => {
  return {
    result: true,
    dataType: 'message',
    data: data
  }
}

// response format: negative
const responseFalse = (error) => {
  return {
    result: false,
    dataType: 'string',
    error: error.message
  }
}

// get all messages
router.get('/', (req, res) =>
  Message.find({}, (err, messages) => {
    if (err) {
      return res.send(responseFalse(err))
    } else {
      res.status(200).send(responseTrue(messages))
    }
  })
)

// create message
router.post('/', (req, res) =>
  Message.create({
    content: req.body.content,
    timestamp: req.body.timestamp,
    tags: req.body.tags
  },
  (err, message) => {
    if (err) {
      return res.send(responseFalse(err))
    } else {
      res.status(200).send(responseTrue(message))
    }
  })
)

// get message
router.get('/:id', (req, res) =>
  Message.findById(req.params.id, (err, message) => {
    if (err) {
      return res.send(responseFalse(err))
    } else {
      res.status(200).send(responseTrue(message))
    }
  })
)

// delete message
router.delete('/:id', (req, res) =>
  Message.findByIdAndRemove(req.params.id, (err) => {
    if (err) {
      return res.send(responseFalse(err))
    } else {
      res.status(200).send(responseTrue('Message deleted'))
    }
  })
)


module.exports = router
