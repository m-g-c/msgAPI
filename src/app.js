const app = require('express')()
const msgController = require('./controllers/msgController')



app.use('/api/messages', msgController)


module.exports = app
