const mongoose = require('mongoose')
const Schema = mongoose.Schema
const timestamp = require('unix-timestamp')
timestamp.round = true
const keys = require('../config/keys')



mongoose.connect(`mongodb://${keys.db.user}:${keys.db.password}@ds241055.mlab.com:41055/msgapi`, { useMongoClient: true })

const MessageSchema = new Schema({
  content: {
    type: String,
    required: true
  },
  timestamp: {
    type: Number,
    default: timestamp.now(),
    required: true,
    validate: {
      validator: function(value) {
        if (value.toString().length === 10) {
          return true
        } else {
          return false
        }
      },
      message: '{VALUE} is not a valid unix epoch formatted timestamp'
    }
  },
  tags: {
    type: [String]
  }
})

mongoose.model('Message', MessageSchema)


module.exports = mongoose.model('Message')
