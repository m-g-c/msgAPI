# msgAPI

Microservice whose role is to manage and store message data objects

## Technologies

- Node.js
- Express
- MongoDB
- Mongoose

## Installation and Launch

- clone or download the repository
- run `npm install`
- add database username and password to the keys.js file in /src/config folder

```javascript
const keys = {
  db: {
    user: '',
    password: ''
  }
}

module.exports = keys
```

- run `npm start` to launch the application
- run `npm run dev` to launch the application with nodemon (see section Possible Issues)

## Examples

### Postman

You can interact with the application via Postman:
- install [Postman](https://www.getpostman.com/)
- launch Postman and enter following address into the address bar `http://localhost:3000/api/messages`
- choose method __GET__ from the selector to the left of the address bar and click on the __Send__ button
- you shall receive a response in the following format:

```javascript
{
    "result": true,
    "dataType": "message",
    "data": [
        {
            "_id": "59f84d8454a30f224cf394f0",
            "content": "message 1",
            "__v": 0,
            "tags": [
                "tag 1",
                "tag 2"
            ],
            "timestamp": 1509444877
        },
        {
            "_id": "59f8641862995f2d344a0070",
            "content": "message 2",
            "__v": 0,
            "tags": [
                "tag 1",
                "tag 2",
                "tag 3"
            ],
            "timestamp": 1509450771
        }
    ]
}
```

- then choose method __POST__, choose tab __Body__, mark radio button __x-www-form-urlencoded__, and finally add key value
pairs to the table below
- for the key __content__ enter eg value __message 3__, and for the key __tags__ you might enter __tag 1__, then in the next row enter
the key __tags__ again with the value __tag 2__ (multiple strings will be added to the __tags__ array)
- expect following response:

```javascript
{
    "result": true,
    "dataType": "message",
    "data": {
        "__v": 0,
        "content": "message 3",
        "_id": "59f84db454a30f224cf394f1",
        "tags": [
            "tag 1",
            "tag 2"
        ],
        "timestamp": 1509444877
    }
}
```

- then copy a value of the message __id__ and add it to the address in such a manner:
`http://localhost:3000/api/messages/<id>`. You can use the following address
`http://localhost:3000/api/messages/59f84db454a30f224cf394f1` then use method
__GET__ to display the message with the content __message 1__. It should provide
following response:

```javascript
{
    "result": true,
    "dataType": "message",
    "data": {
        "_id": "59f84d8454a30f224cf394f0",
        "content": "message 1",
        "__v": 0,
        "tags": [
            "tag 1",
            "tag 2"
        ],
        "timestamp": 1509444877
    }
}
```

- finally, using the same address, use the __DELETE__ method to delete the message. It will result in the following response:

```javascript
{
    "result": true,
    "dataType": "message",
    "data": "Message deleted"
}
```

## Possible Issues

### Issue with nodemon

It is possible that __Internal watch failed: watch ENOSPC__ error will occur while launching the application with nodemon.
It is possible to resolve it in such a way:
`echo fs.inotify.max_user_watches=582222 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p`.
For further reference visit following links:
- https://github.com/remy/nodemon/issues/214
- https://stackoverflow.com/questions/34662574/node-js-getting-error-nodemon-internal-watch-failed-watch-enospc
